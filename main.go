package main

import (
	"flag"
	"fmt"
	"os"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/ernado/zero"
)

// ErrorPrefix for error messages
const ErrorPrefix = "error: %v\n"

func checkForInstances(client *ec2.EC2, VpcID string, region string) {
	instances, _ := client.DescribeInstances(&ec2.DescribeInstancesInput{})
	if len(instances.Reservations) > 0 {
		for _, instance := range instances.Reservations {
			if *instance.Instances[0].State.Name != "terminated" {
				fmt.Printf("EC2 instance detected in vpc: %v, region %v\nStopping execution!\n", VpcID, region)
				os.Exit(1)
			}
		}
	}
}

func returnDefaultVpcForRegion(client *ec2.EC2) string {
	v, err := client.DescribeVpcs(&ec2.DescribeVpcsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("isDefault"),
				Values: []*string{aws.String("true")},
			},
		},
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, ErrorPrefix, err)
	}

	result := ""
	if !zero.IsZero(*v) {
		result = *v.Vpcs[0].VpcId
	}
	return result
}

func deleteIgw(client *ec2.EC2, VpcID string, region string, DryRun bool) string {
	var result string
	igws, _ := client.DescribeInternetGateways(&ec2.DescribeInternetGatewaysInput{})
	for _, igw := range igws.InternetGateways {
		if *igw.Attachments[0].VpcId == VpcID {
			_, detacherr := client.DetachInternetGateway(&ec2.DetachInternetGatewayInput{
				InternetGatewayId: aws.String(*igw.InternetGatewayId),
				VpcId:             aws.String(VpcID),
				DryRun:            aws.Bool(DryRun),
			})
			if detacherr != nil {
				fmt.Fprintf(os.Stderr, ErrorPrefix, detacherr)
			}

			_, err := client.DeleteInternetGateway(&ec2.DeleteInternetGatewayInput{
				InternetGatewayId: aws.String(*igw.InternetGatewayId),
				DryRun:            aws.Bool(DryRun),
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, ErrorPrefix, err)
			}
		}
		result = fmt.Sprintf("Delete IGW: %v from vpc %v in region %v", *igw.InternetGatewayId, VpcID, region)
	}
	return result
}

func deleteSubnets(client *ec2.EC2, VpcID string, region string, DryRun bool) string {
	subnets, _ := client.DescribeSubnets(&ec2.DescribeSubnetsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("vpc-id"),
				Values: []*string{aws.String(VpcID)},
			},
		},
	})
	if len(subnets.Subnets) > 0 {
		for _, subnet := range subnets.Subnets {
			_, err := client.DeleteSubnet(&ec2.DeleteSubnetInput{
				SubnetId: aws.String(*subnet.SubnetId),
				DryRun:   aws.Bool(DryRun),
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, ErrorPrefix, err)
			}
		}
	}
	return fmt.Sprintf("Deleted subnets for vpc %v in region %v", VpcID, region)
}

func deleteRouteTables(client *ec2.EC2, VpcID string, region string, DryRun bool) string {
	routeTables, _ := client.DescribeRouteTables(&ec2.DescribeRouteTablesInput{})
	for _, routeTable := range routeTables.RouteTables {
		// Skip route table of type main
		if len(routeTable.Associations) > 0 && *routeTable.Associations[0].Main {
			continue
		}

		_, err := client.DeleteRouteTable(&ec2.DeleteRouteTableInput{
			RouteTableId: aws.String(*routeTable.RouteTableId),
			DryRun:       aws.Bool(DryRun),
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, ErrorPrefix, err)
		}
	}
	return fmt.Sprintf("Deleted subnets for vpc %v in region %v", VpcID, region)

}

func deleteNetworkACLs(client *ec2.EC2, VpcID string, region string, DryRun bool) string {
	networkACLs, _ := client.DescribeNetworkAcls(&ec2.DescribeNetworkAclsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("default"),
				Values: []*string{aws.String("false")},
			},
			&ec2.Filter{
				Name:   aws.String("vpc-id"),
				Values: []*string{aws.String(VpcID)},
			},
		},
	})
	for _, networkACL := range networkACLs.NetworkAcls {
		_, err := client.DeleteNetworkAcl(&ec2.DeleteNetworkAclInput{
			NetworkAclId: aws.String(*networkACL.NetworkAclId),
			DryRun:       aws.Bool(DryRun),
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, ErrorPrefix, err)
		}
	}
	return fmt.Sprintf("Delete network acls for vpc %v in region %v", VpcID, region)
}

func deleteSecurityGroups(client *ec2.EC2, VpcID string, region string, DryRun bool) string {
	securityGroups, _ := client.DescribeSecurityGroups(&ec2.DescribeSecurityGroupsInput{
		Filters: []*ec2.Filter{
			&ec2.Filter{
				Name:   aws.String("vpc-id"),
				Values: []*string{aws.String(VpcID)},
			},
		},
	})
	for _, securityGroup := range securityGroups.SecurityGroups {
		if *securityGroup.GroupName != "default" {
			_, err := client.DeleteSecurityGroup(&ec2.DeleteSecurityGroupInput{
				GroupId: aws.String(*securityGroup.GroupId),
				DryRun:  aws.Bool(DryRun),
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, ErrorPrefix, err)
			}
		}
	}
	return fmt.Sprintf("Delete SGs for vpc %v in region %v", VpcID, region)
}

func deleteVpc(client *ec2.EC2, VpcID string, region string, DryRun bool) string {
	_, err := client.DeleteVpc(&ec2.DeleteVpcInput{
		VpcId:  aws.String(VpcID),
		DryRun: aws.Bool(DryRun),
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, ErrorPrefix, err)
	}
	return fmt.Sprintf("Delete Vpc: %v in region %v", VpcID, region)
}

func nukeDefaultVpcs(sess *session.Session, region string, c chan string, w *sync.WaitGroup, DryRun bool) {
	defer w.Done()
	svc := ec2.New(sess)
	VpcID := returnDefaultVpcForRegion(svc)
	if VpcID != "" {
		checkForInstances(svc, VpcID, region)
		c <- deleteIgw(svc, VpcID, region, DryRun)
		c <- deleteSubnets(svc, VpcID, region, DryRun)
		c <- deleteRouteTables(svc, VpcID, region, DryRun)
		c <- deleteNetworkACLs(svc, VpcID, region, DryRun)
		c <- deleteSecurityGroups(svc, VpcID, region, DryRun)
		c <- deleteVpc(svc, VpcID, region, DryRun)
	} else {
		// fmt.Printf("No default vpc in region %v\n", *region.RegionName)
		c <- fmt.Sprintf("No default vpc in region %v", region)
	}
}

func getRegions(client *ec2.EC2) []string {
	regions, err := client.DescribeRegions(&ec2.DescribeRegionsInput{})
	if err != nil {
		fmt.Fprintf(os.Stderr, ErrorPrefix, err)
	}
	var result []string
	for _, region := range regions.Regions {
		result = append(result, *region.RegionName)
	}
	return result
}

func main() {
	// Define command line falgs
	dryRun := flag.Bool("dryrun", false, "Test without running")
	profile := flag.String("profile", "", "AWS profile name")

	flag.Parse()

	// Define required parameters
	if *profile == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	// Create session
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config:                  aws.Config{Region: aws.String("eu-west-1")},
		Profile:                 *profile,
		SharedConfigState:       session.SharedConfigEnable,
		AssumeRoleTokenProvider: stscreds.StdinTokenProvider,
	}))

	_, err := sess.Config.Credentials.Get()
	if err != nil {
		fmt.Fprintf(os.Stderr, ErrorPrefix, err)
		os.Exit(1)
	}

	// Create ec2 client
	svc := ec2.New(sess)
	if err != nil {
		fmt.Fprintf(os.Stderr, ErrorPrefix, err)
	}
	r := getRegions(svc)
	responses := make(chan string)
	var wg sync.WaitGroup
	wg.Add(len(r))
	for _, region := range r {
		// Set session to specified AWS region
		sess := sess.Copy(&aws.Config{
			Region: aws.String(region),
		})

		go nukeDefaultVpcs(sess, region, responses, &wg, *dryRun)
	}

	go func() {
		for response := range responses {
			fmt.Println(response)
		}
	}()

	wg.Wait()
}
