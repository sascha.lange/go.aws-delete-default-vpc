#!/usr/bin/env bash
docker build -t builder .
docker run -v $(pwd):/go/src/app -ti builder env GOOS=darwin  go build -ldflags="-s -w" -o delete-default-vpc-darwin main.go
docker run -v $(pwd):/go/src/app -ti builder env GOOS=linux  go build -ldflags="-s -w" -o delete-default-vpc-linux main.go
docker run -v $(pwd):/go/src/app -ti builder env GOOS=windows  go build -ldflags="-s -w" -o delete-default-vpc-windows.exe main.go
