package main

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

func main() {
	// Create session
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		Config:  aws.Config{Region: aws.String("eu-west-1")},
		Profile: "mylab",
	}))
	svc := ec2.New(sess)

	instances, _ := svc.DescribeInstances(&ec2.DescribeInstancesInput{})
	fmt.Println(instances)
}
