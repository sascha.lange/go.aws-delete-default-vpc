FROM golang:1.12.7-alpine as builder

RUN apk add --no-cache git upx

WORKDIR /go/src/app

COPY . .

RUN go get -d -v ./...

#RUN GOOS=linux go build -ldflags="-s -w" -o delete-default-vpc-linux main.go && \
#    upx delete-default-vpc-linux
#
#RUN GOOS=darwin go build -ldflags="-s -w" -o delete-default-vpc-darwin main.go && \
#    upx delete-default-vpc-darwin

