.PHONY: build clean

build:
	go get github.com/aws/aws-sdk-go
	go get github.com/ernado/zero
	env GOOS=linux go build -ldflags="-s -w" -o  bin/delete-default-vpc-linux-amd64 main.go
	upx bin/delete-default-vpc-linux-amd64
	env GOOS=darwin go build -ldflags="-s -w" -o  bin/delete-default-vpc-darwin-amd64 main.go
	upx bin/delete-default-vpc-darwin-amd64
	env GOOS=windows go build -ldflags="-s -w" -o  bin/delete-default-vpc-windows-amd64.exe main.go
	upx bin/delete-default-vpc-windows-amd64.exe

clean:
	rm -rf ./bin
