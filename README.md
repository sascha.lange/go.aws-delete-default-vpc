# Description
_THIS IS CURRENTLY WORK IN PROGRESS!_

A small script that will delete the default vpc in all regions, use it with caution.

It will ask for a aws profile name to use for authentication, since we are not using the default profile as default.

Before deleting anything it will check for ec2 instances. If there are ec2 instances deployed it will exit.

If you want to shrink down the binary with upx you need to install it first. Visit the projects homepage for more information. https://upx.github.io/

Install on Mac OS with brew:
```shell
$ brew install upx
```

# Build
## Default build command
```shell
$ go build -o delete-default-vpc .
```

## Build command that removes DWARF tables
_DWARF tables are needed for debuggers!_
```shell
$ go build -ldflags="-s -w" -o delete-default-vpc .
```

## Shrink the binary
```shell
$ upx <your binary>
```

## Shrink the binary even more.
_This is a slow process!_
```shell
$ upx --brute <your binary>
```

# Usage
## Print help
```shell
$ ./delete-default-vpc -h
```

## Dry run without deleting anyting.
_Can be used to test permissions or to see what is going to be deleted!_
```shell
$ ./delete-default-vpc -dryrun -profile <aws profile name>
```

## Delete default vpc in every region
_Point of no return!_
```shell
$ ./delete-default-vpc -profile <aws profile name>
```

# ToDo
- Implement go tests
- Remove dependency to "github.com/ernado/zero"
- Add aws-sdk-go error handling
- Implement proper result from client actions
